<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Marcin Szczęsny',
            'email' => 'gambeat@poczta.onet.pl',
            'password' => bcrypt('qwerty'),
			'created_at' => new DateTime,
        ]);
		
		DB::table('users')->insert([
            'name' => 'Jan Testowy',
            'email' => 'admin@admin.pl',
            'password' => bcrypt('qwerty'),
			'created_at' => new DateTime,
        ]);
    }
}
