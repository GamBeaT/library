<?php

Route::group(['prefix' => 'categories'], function() {
	Route::get('/', [
		'as' => 'categories.list',
		'uses' => 'Books\CategoriesController@index',
	]);
	Route::get('add', [
		'as' => 'categories.add.get',
		'uses' => 'Books\CategoriesController@addCategoryForm',
	]);
	Route::post('add', [
		'as' => 'categories.add.post',
		'uses' => 'Books\CategoriesController@addCategory',
	]);
	Route::get('edit/{id?}', [
		'as' => 'categories.edit.get',
		'uses' => 'Books\CategoriesController@editCategoryForm',
	]);
	Route::post('edit', [
		'as' => 'categories.edit.post',
		'uses' => 'Books\CategoriesController@editCategory',
	]);
	Route::post('delete', [
		'as' => 'categories.delete',
		'uses' => 'Books\CategoriesController@deleteCategory',
	]);
});