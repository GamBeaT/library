<?php

Route::group(['prefix' => 'books'], function() {
	Route::get('/', [
		'as' => 'books.list',
		'uses' => 'Books\BooksController@index',
	]);
	Route::get('add', [
		'as' => 'books.add.get',
		'uses' => 'Books\BooksController@addBookForm',
	]);
	Route::post('add', [
		'as' => 'books.add.post',
		'uses' => 'Books\BooksController@addBook',
	]);
	Route::get('edit/{id?}', [
		'as' => 'books.edit.get',
		'uses' => 'Books\BooksController@editBookForm',
	]);
	Route::post('edit', [
		'as' => 'books.edit.post',
		'uses' => 'Books\BooksController@editBook',
	]);
	Route::post('delete', [
		'as' => 'books.delete',
		'uses' => 'Books\BooksController@deleteBook',
	]);
	Route::post('status', [
		'as' => 'books.status',
		'uses' => 'Books\BooksController@changeStatus',
	]);
});