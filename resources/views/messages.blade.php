<div class="modal fade" tabindex="-1" role="dialog" id="modalError">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body bg-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br />
				@endforeach
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalSuccess">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body bg-success">
				@if (Session::has('success'))
					{{ Session::get('success') }}
				@endif
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		@if ($errors->count() > 0)
			$('#modalError').modal('show');
		@endif
		@if (Session::has('success'))
			$('#modalSuccess').modal('show');
		@endif
	});
</script>
