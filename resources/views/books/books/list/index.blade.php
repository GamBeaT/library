@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="page-header">
			<h1>Books list</h1>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Books list
				<div class="pull-right">
					<a class="btn btn-info btn-xs" href="{{ route('books.add.get') }}">Add book</a>
				</div>
			</div>
			@if ($books->isEmpty())
			<div class="panel-body">
				No books added
			</div>
			@else
			<table class="table">
				<thead>
					<tr>
						<th class="col-md-1">#</th>
						<th>Title</th>
						<th>Category</th>
						<th class="col-md-1">ISBN</th>
						<th class="col-md-1">Status</th>
						<th class="col-md-1">Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($books as $key => $book)
					<tr>
						<td>{{ $key+1 }}</td>
						<td>{{ $book->title }}</td>
						<td>{{ $book->categories->title }}</td>
						<td>{{ $book->isbn }}</td>
						<td>
							<form class="form" method="post" action="{{ route('books.status') }}">
								{{ csrf_field() }}
								<input type="hidden" name="id" value={{ $book->id }}">
								@if ($book->status)
									<input type="hidden" name="status" value="0">
									<button class="btn btn-default btn-xs" type="submit">
										<i class="fa fa-check text-success"></i>
									</button>
								@else
									<input type="hidden" name="status" value="1">
									<button class="btn btn-default btn-xs" type="submit">
										<i class="fa fa-times text-danger"></i>
									</button>
								@endif
							</form>
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Actions <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="{{ route('books.edit.get',$book->id) }}">Edit</a></li>
									@if ($book->status == 1)
									<li><a href="#" class="bg-danger delete" data-id="{{ $book->id }}" onclick="event.preventDefault();">Delete</a></li>
									@endif
								</ul>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<form id="delete-form" action="{{ route('books.delete') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
				<input type="hidden" name="id" id="id" value="">
			</form>
			@endif
		</div>
	</div>
</div>
@endsection
@section('jquery')
<script type="text/javascript">
	$('.delete').click(function() {
		var id = $(this).data("id");
		if (confirm('Are you sure ?')) {
			$('#id').val(id);
			$('#delete-form').submit();
		}
	});
</script>
@endsection
