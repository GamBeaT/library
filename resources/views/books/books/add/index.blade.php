@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="page-header">
			<h1>Adding book</h1>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Form
			</div>
			<form class="form-horizontal" role="form" method="post" action="{{ route('books.add.post') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="panel-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title') }}">
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-sm-2 control-label">Category <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<select class="form-control" id="category" name="category">
								<option>---</option>
								@if (!$categories->isEmpty())
								@foreach ($categories as $category)
								<option value="{{ $category->id }}" @if (old('category') == $category->id)selected @endif>{{ $category->title }}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="isbn" class="col-sm-2 control-label">ISBN <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="isbn" name="isbn" placeholder="ISBN" value="{{ old('isbn') }}" maxLength="13">
						</div>
					</div>
					<div class="form-group">
						<label for="image" class="col-sm-2 control-label">Image <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="file" class="btn btn-default btn-xs" id="image" name="image" placeholder="ISBN" value="{{ old('isbn') }}" maxLength="13" accept="image/*">
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-danger" type="submit">
						Add
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
