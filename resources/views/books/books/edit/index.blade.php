@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="page-header">
			<h1>Editing book</h1>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Form
			</div>
			<form class="form-horizontal" role="form" method="post" action="{{ route('books.edit.post') }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ $book->id }}">
				<div class="panel-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="@if (old('title')){{ old('title') }}@else{{ $book->title }}@endif">
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-sm-2 control-label">Category <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<select class="form-control" id="category" name="category">
								<option>---</option>
								@if (!$categories->isEmpty())
								@foreach ($categories as $category)
								<option value="{{ $category->id }}" @if (old('category'))@if(old('category') == $category->id) selected @endif @else @if ($book->category == $category->id)selected @endif @endif>{{ $category->title }}</option>
								@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="isbn" class="col-sm-2 control-label">ISBN <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="isbn" name="isbn" placeholder="ISBN" value="@if (old('isbn')){{ old('isbn') }}@else{{ $book->isbn }}@endif" maxLength="13">
						</div>
					</div>
					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Status <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<select class="form-control" id="status" name="status">
								<option value="1" @if (old('status'))@if(old('status') == "1") selected @endif @else @if ($book->status == 1)selected @endif @endif>Avaliable</option>
								<option value="0" @if (old('status'))@if(old('status') == "0") selected @endif @else @if ($book->status == 0)selected @endif @endif>Borrowed</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="image" class="col-sm-2 control-label">Image</label>
						<div class="col-sm-10">
							<input type="file" class="btn btn-default btn-xs" id="image" name="image" placeholder="ISBN" value="{{ old('isbn') }}" maxLength="13" accept="image/*">
						</div>
					</div>
					<div class="form-group">
						<label for="image" class="col-sm-2 control-label">Preview</label>
						<div class="col-sm-10">
							<img src="{{ url('storage/'.$book->image) }}" style="width: 400px; height: 300px;">
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-danger" type="submit">
						Edit
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
