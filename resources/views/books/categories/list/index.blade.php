@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="page-header">
			<h1>Categories list</h1>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Categories list
				<div class="pull-right">
					<a class="btn btn-info btn-xs" href="{{ route('categories.add.get') }}">Add category</a>
				</div>
			</div>
			@if ($categories->isEmpty())
			<div class="panel-body">
				No categories added
			</div>
			@else
			<table class="table">
				<thead>
					<tr>
						<th class="col-md-1">#</th>
						<th>Title</th>
						<th class="col-md-1">Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($categories as $key => $category)
					<tr>
						<td>{{ $key+1 }}</td>
						<td>{{ $category->title }}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Actions <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="{{ route('categories.edit.get',$category->id) }}">Edit</a></li>
									@if ($category->books->count() == 0)
									<li><a href="#" class="bg-danger delete" data-id="{{ $category->id }}" onclick="event.preventDefault();">Delete</a></li>
									@endif
								</ul>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<form id="delete-form" action="{{ route('categories.delete') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
				<input type="hidden" name="id" id="id" value="">
			</form>
			@endif
		</div>
	</div>
</div>
@endsection
@section('jquery')
<script type="text/javascript">
	$('.delete').click(function() {
		var id = $(this).data("id");
		if (confirm('Are you sure ?')) {
			$('#id').val(id);
			$('#delete-form').submit();
		}
	});
</script>
@endsection