@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="page-header">
			<h1>Adding category</h1>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Form
			</div>
			<form class="form-horizontal" role="form" method="post" action="{{ route('categories.add.post') }}">
				{{ csrf_field() }}
				<div class="panel-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title <i class="fa fa-asterisk text-danger"></i></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title') }}">
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-danger" type="submit">
						Add
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection