<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		Books <span class="caret"></span>
	</a>
	<ul class="dropdown-menu" role="menu">
		<li>
			<a href="{{ route('categories.list') }}">
                Categories
			</a>
		</li>
		<li>
			<a href="{{ route('books.list') }}">
                Books
			</a>
		</li>
    </ul>
</li>
