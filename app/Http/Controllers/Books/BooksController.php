<?php

namespace App\Http\Controllers\Books;

use Illuminate\Http\Request;

use App\Models\Book;
use App\Models\Category;
use App\Http\Controllers\Controller;

use Storage;
use Validator;

class BooksController extends Controller
{
	protected $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		$this->data['books'] = Book::orderBy('title','asc')
			->with(['categories'])
			->get();
        return view('books.books.list.index')
			->with($this->data);
    }
	
	public function addBookForm()
	{
		$this->data['categories'] = Category::orderBy('title','asc')->get();
		return view('books.books.add.index')
			->with($this->data);
	}
	
	public function addBook(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'title' => 'required|unique:books,title',
				'category' => 'required',
				'isbn' => 'required|min:10|max:13|unique:books,isbn',
				'image' => 'required|image',
			]
		);
		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput($request->all());
		} else {
			$book = new Book;
			$book->title = $request->title;
			$book->category = $request->category;
			$book->isbn = $request->isbn;
			$path = $request->image->store('public/books/images');
			$path = str_replace('public/','',$path);
			$book->image = $path;
			$book->save();
			return redirect()
				->route('books.list')
				->with('success','Book added');
		}
	}
	
	public function editBookForm($id = null)
	{
		$this->data['book'] = Book::where('id','=',$id)->first();
		if ($this->data['book'] || !empty($id)) {
			$this->data['categories'] = Category::orderBy('title','asc')->get();
			return view('books.books.edit.index')
				->with($this->data);
		} else {
			$error = [
				'id' => 'Book not found',
			];
			return redirect()
				->route('categories.list')
				->withErrors($error);
		}
	}
	
	public function editBook(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'title' => 'required|unique:books,title,'.$request->id,
				'category' => 'required',
				'isbn' => 'required|min:10|max:13|unique:books,isbn,'.$request->id,
				'status' => 'required',
				'image' => 'image',
			]
		);
		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput($request->all());
		} else {
			$book = Book::where('id','=', $request->id)->first();
			$book->title = $request->title;
			$book->category = $request->category;
			$book->isbn = $request->isbn;
			$book->status = $request->status;
			if ($request->hasFile('image')) {
				Storage::delete('public/'.$book->image);
				$path = $request->image->store('public/books/images');
				$path = str_replace('public/','',$path);
				$book->image = $path;
			}
			$book->save();
			return redirect()
				->back()
				->with('success','Book edited');
		}
	}
	
	public function deleteBook(Request $request)
	{
		if ($request->id) {
			$book = Book::where('id','=',$request->id)->first();
			Storage::delete('public/'.$book->image);
			$book->delete();
			return redirect()
				->back()
				->with('success','Book deleted');
		} else {
			$error = [
				'id' => 'Book not found',
			];
			return redirect()
				->back()
				->withErrors($error);
		}
	}

	public function changeStatus(Request $request)
	{
		if ($request->id) {
			$book = Book::where('id','=',$request->id)->first();
			$book->status = $request->status;
			$book->save();
			return redirect()
				->back()
				->with('success','Book status changed');
		} else {
			$error = [
				'id' => 'Book not found',
			];
			return redirect()
				->back()
				->withErrors($error);
		}
	}
}
