<?php

namespace App\Http\Controllers\Books;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Http\Controllers\Controller;

use Validator;

class CategoriesController extends Controller
{
	protected $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$this->data['categories'] = Category::orderBy('title','asc')
			->with(['books'])
			->get();
        return view('books.categories.list.index')
			->with($this->data);
    }
	
	public function addCategoryForm()
	{
		return view('books.categories.add.index');
	}
	
	public function addCategory(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'title' => 'required|unique:categories,title',
			]
		);
		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput($request->all());
		} else {
			$category = new Category;
			$category->title = $request->title;
			$category->save();
			return redirect()
				->route('categories.list')
				->with('success','Category added');
		}
	}
	
	public function editCategoryForm($id = null)
	{
		$this->data['category'] = Category::where('id','=',$id)->first();
		if ($this->data['category'] || !empty($id)) {
			return view('books.categories.edit.index')
				->with($this->data);
		} else {
			$error = [
				'id' => 'Category not found',
			];
			return redirect()
				->route('categories.list')
				->withErrors($error);
		}
	}
	
	public function editCategory(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'title' => 'required|unique:categories,title,'.$request->id,
			]
		);
		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput($request->all());
		} else {
			$category = Category::where('id','=',$request->id)->first();
			$category->title = $request->title;
			$category->save();
			return redirect()
				->back()
				->with('success','Category edited');
		}
	}
	
	public function deleteCategory(Request $request)
	{
		if ($request->id) {
			$category = Category::where('id','=',$request->id)->first();
			$category->delete();
			return redirect()
				->back()
				->with('success','Category deleted');
		} else {
			$error = [
				'id' => 'Category not found',
			];
			return redirect()
				->back()
				->withErrors($error);
		}
	}
}
