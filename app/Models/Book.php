<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
	
	public function categories()
	{
		return $this->belongsTo('App\Models\Category','category');
	}
}
